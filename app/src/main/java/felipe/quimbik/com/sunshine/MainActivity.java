package felipe.quimbik.com.sunshine;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;


public class MainActivity extends ActionBarActivity implements ItemClickCallback {

    private boolean mTwoPane = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (findViewById(R.id.weather_detail_container) != null) {
            mTwoPane = true;

            if (savedInstanceState == null) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.weather_detail_container, new DetailFragment())
                        .commit();
            }
        } else {
            mTwoPane = false;

        }
        ForecastFragment forecastFragment = ((ForecastFragment) getSupportFragmentManager().
                findFragmentById(R.id.container));
        forecastFragment.setUseTodayLayout(!mTwoPane);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent myIntent = new Intent(getApplicationContext(), SettingsActivity.class);
            startActivity(myIntent);
            return true;
        }
        if (id == R.id.user_location) {
            openUserLocationOnMap();
        }
        return super.onOptionsItemSelected(item);
    }

    private void openUserLocationOnMap() {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String location = pref.getString(getString(R.string.pref_location_key), getString(R.string.pref_default_location_key));
        Uri geoLocation = Uri.parse("geo:0,0?").buildUpon().appendQueryParameter("q", location).build();

        Intent myIntent = new Intent(Intent.ACTION_VIEW);
        myIntent.setData(geoLocation);
        startActivity(myIntent);
    }

    @Override
    public void onItemSelected(String date) {
        if (mTwoPane) {
            Bundle myBundle = new Bundle();
            myBundle.putString(DetailFragment.DATE_KEY, date);
            DetailFragment fragment = new DetailFragment();
            fragment.setArguments(myBundle);
            getSupportFragmentManager().beginTransaction().
                    replace(R.id.weather_detail_container, fragment).commit();
        }else {
            Intent intent = new Intent(this, DetailActivity.class);
            intent.putExtra(DetailFragment.DATE_KEY, date);
            startActivity(intent);
        }

    }

}
