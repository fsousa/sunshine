package felipe.quimbik.com.sunshine;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.ShareActionProvider;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import felipe.quimbik.com.sunshine.data.WeatherContract;
import felipe.quimbik.com.sunshine.data.WeatherContract.WeatherEntry;

/**
 * Created by felipe on 9/28/14.
 */
public class DetailFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final String FORECAST_HASH_TAG = " #SunshineApp";
    private static final int DETAIL_LOADER = 0;
    public static final String DATE_KEY = "date";
    public static final String LOCATION_KEY = "location";

    private static final String[] FORECAST_COLUMNS = {
            WeatherEntry.TABLE_NAME + "." + WeatherEntry._ID,
            WeatherEntry.COLUMN_DATETEXT,
            WeatherEntry.COLUMN_SHORT_DESC,
            WeatherEntry.COLUMN_MAX_TEMP,
            WeatherEntry.COLUMN_MIN_TEMP,
            WeatherEntry.COLUMN_HUMIDITY,
            WeatherEntry.COLUMN_PRESSURE,
            WeatherEntry.COLUMN_WIND_SPEED,
            WeatherEntry.COLUMN_DEGREES,
            WeatherEntry.COLUMN_WEATHER_ID,
            WeatherContract.LocationEntry.COLUMN_LOCATION_SETTING
    };

    public DetailFragment() {
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.detail_fragment, menu);
        MenuItem menuItem = menu.findItem(R.id.menu_item_share);
        ShareActionProvider mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(menuItem);
        if (mShareActionProvider != null) {
            mShareActionProvider.setShareIntent(createShareForecastIntent());
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle myBundle = getArguments();
        if(myBundle != null && myBundle.containsKey(DATE_KEY)) {
            getLoaderManager().initLoader(DETAIL_LOADER, null, this);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_detail, container, false);

        return rootView;
    }

    private Intent createShareForecastIntent() {
        Intent myIntent = new Intent(Intent.ACTION_SEND);
        myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        myIntent.setType("text/plain");
        myIntent.putExtra(Intent.EXTRA_TEXT, FORECAST_HASH_TAG);
        return myIntent;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        Bundle data = getArguments();
        if (data == null || data.getString(DATE_KEY).equals("")) {
            return null;
        }
        String forecastDate = data.getString(DATE_KEY);

        // Sort order:  Ascending, by date.
        String sortOrder = WeatherContract.WeatherEntry.COLUMN_DATETEXT + " ASC";

        Uri weatherForLocationUri = WeatherContract.WeatherEntry.buildWeatherLocationWithDate(
                Utility.getPreferredLocation(getActivity()), forecastDate);

        // Now create and return a CursorLoader that will take care of
        // creating a Cursor for the data being displayed.
        return new CursorLoader(
                getActivity(),
                weatherForLocationUri,
                FORECAST_COLUMNS,
                null,
                null,
                sortOrder
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor data) {
        if (!data.moveToFirst()) {
            return;
        }

        int weatherId = data.getInt(data.getColumnIndex(WeatherEntry.COLUMN_WEATHER_ID));

        String dateString =
                data.getString(data.getColumnIndex(WeatherEntry.COLUMN_DATETEXT));
        String weatherDescription =
                data.getString(data.getColumnIndex(WeatherEntry.COLUMN_SHORT_DESC));
        String friendlyDateText = Utility.getFormattedMonthDay(getActivity(), dateString);
        String dayText = Utility.getFriendlyDayString(getActivity(), dateString);

        boolean isMetric = Utility.isMetric(getActivity());
        String high = Utility.formatTemperature(getActivity().getApplicationContext(),
                data.getDouble(data.getColumnIndex(WeatherEntry.COLUMN_MAX_TEMP)), isMetric);
        String low = Utility.formatTemperature(getActivity().getApplicationContext(),
                data.getDouble(data.getColumnIndex(WeatherEntry.COLUMN_MIN_TEMP)), isMetric);
        float humidity = data.getFloat(data.getColumnIndex(WeatherEntry.COLUMN_HUMIDITY));
        float windSpeed = data.getFloat(data.getColumnIndex(WeatherEntry.COLUMN_WIND_SPEED));
        float windDre = data.getFloat(data.getColumnIndex(WeatherEntry.COLUMN_DEGREES));
        float pressure = data.getFloat(data.getColumnIndex(WeatherEntry.COLUMN_PRESSURE));

        View fragmentView = getView();
        if (fragmentView != null) {

            TextView tvDate = (TextView) fragmentView.findViewById(R.id.details_date_textview);
            TextView tvFriendly = (TextView) fragmentView.findViewById(R.id.details_friendly_textview);
            TextView tvWeather = (TextView) fragmentView.findViewById(R.id.details_forecast_textview);
            TextView tvHigh = (TextView) fragmentView.findViewById(R.id.details_high_textview);
            TextView tvLow = (TextView) fragmentView.findViewById(R.id.details_low_textview);
            TextView tvHumidityView = (TextView) fragmentView.findViewById(R.id.details_humitiy_textview);
            TextView tvWindView = (TextView) fragmentView.findViewById(R.id.details_wind_textview);
            TextView tvPressure = (TextView) fragmentView.findViewById(R.id.details_pressure_textview);
            ImageView ivIcon = (ImageView) fragmentView.findViewById(R.id.details_icon);

            tvDate.setText(dayText);
            tvFriendly.setText(friendlyDateText);
            tvWeather.setText(weatherDescription);
            tvHigh.setText(high);
            tvLow.setText(low);
            tvHumidityView.setText(getActivity().getString(R.string.format_humidity, humidity));
            tvWindView.setText(Utility.getFormattedWind(getActivity(), windSpeed, windDre));
            tvPressure.setText(getActivity().getString(R.string.format_pressure, pressure));
            ivIcon.setImageResource(Utility.getArtResourceForWeatherCondition(weatherId));

        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        getLoaderManager().restartLoader(DETAIL_LOADER, null, this);
    }
}