package felipe.quimbik.com.sunshine;

/**
 * Created by felipe on 9/29/14.
 */
public interface ItemClickCallback {

    public void onItemSelected(String date);

}
